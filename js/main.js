// TABS MAIN TABLE
$('input[name="switch-tab"]').on('change', function () {
    if ($(this).val() == 'tab-1') {
        $(".add-customer").addClass("disN");
        $(".btns-tr-dw").removeClass("disN");
        $("#my_lifting_rings").removeClass("disN");
        $("#my_invoices").addClass("disN");
        $("#lifting_rings_history").addClass("disN");
        $("#my_customers").addClass("disN");

    }
    if ($(this).val() == 'tab-2') {
        $(".add-customer").addClass("disN");
        $(".btns-tr-dw").addClass("disN");
        $("#my_lifting_rings").addClass("disN");
        $("#my_invoices").removeClass("disN");
        $("#lifting_rings_history").addClass("disN");
        $("#my_customers").addClass("disN");

    }
    if ($(this).val() == 'tab-3') {
        $(".add-customer").addClass("disN");
        $(".btns-tr-dw").removeClass("disN");
        $("#my_lifting_rings").addClass("disN");
        $("#my_invoices").addClass("disN");
        $("#lifting_rings_history").removeClass("disN");
        $("#my_customers").addClass("disN");

    }
    if ($(this).val() == 'tab-4') {
        $(".add-customer").removeClass("disN");
        $(".btns-tr-dw").addClass("disN");
        $("#my_lifting_rings").addClass("disN");
        $("#my_invoices").addClass("disN");
        $("#lifting_rings_history").addClass("disN");
        $("#my_customers").removeClass("disN");

    }
    // MOBILE HEADER LIFTING RING INFO RADIO TABS
    if ($(this).val() == 'lifting_tab-1') {
        $(".inf-left-side").removeClass("disN-mob");
        $(".inf-right-side").addClass("disN-mob");
    }
    if ($(this).val() == 'lifting_tab-2') {
        $(".inf-left-side").addClass("disN-mob");
        $(".inf-right-side").removeClass("disN-mob");
    }
});

// MOBILE HEADER WITHS BURGER MENU
$(function(){
    $(".burger-menu").on("click", function(){
        $(".open-menu").removeClass("disN");
    });
    $(".burger-menu-close").on("click", function(){
        $(".open-menu").addClass("disN");
    });
    $(".button-back-menu").on("click", function(){
        $(".open-menu").addClass("disN");
    });
});

// Language select
$(function(){
    $(".select-menu-lang-input").on("click", function(){
        $(".select-menu-lang, .select-menu-b-lang").removeClass("disN");
        $(".select-menu-overlay").removeClass("disN");
    });
    $(".select-menu-overlay, .select-item-lang").on("click", function(){
        $(".select-menu-lang, .select-menu-b-lang").addClass("disN");
        $(".select-menu-overlay").addClass("disN");
    });
});

// My invoice Swivel card more
$(function(){
    $(".swivel-more").on("click", function(){
        $(".swivel-more-block").removeClass("disN");
        $(".swivel-more-arr").addClass("swivel-more-arr-hide");
        $(".swivel-more").removeClass("swivel-more-hide");
    });
    $(".swivel-more-arr-hide").on("click", function(){
        $(".swivel-more-block").addClass("disN");
    });

});
